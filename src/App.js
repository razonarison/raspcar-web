/** 
Mahefa Razonarison
15/03/2021 at 10:58
*/

import react, { useEffect, useState } from "react";
import logo from './logo.svg';
import './App.css';

const ENDPOINT = "ws://localhost:8081";
const client = new WebSocket(ENDPOINT);


function App() {
  const [response, setResponse] = useState("");  

  useEffect(() => {
    client.onopen = () => console.log("CONNECTED");

    client.onerror = (error) => {
      console.log(error);
    }

    client.onmessage = function(message) {
      console.log("MAKATO ---- ");
      let data = JSON.parse(message.data);
      console.log(data);
      // console.log(message.data.checkType);
      setResponse(data.checkType);
    };
  });  

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          { response }
        </a>
      </header>
    </div>
  );
}

export default App;